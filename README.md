# video-content

Identify the `video-content` element inside the DOM and render the video and its contents.

## Install
1. Add package to dependencies `package.json`

    ```
    "dependencies": {
        "video-content": "git+git@bitbucket.org:infomedica/video-content.git#0.0.6"
    }
    ```

2. Run `npm install`

3. Add package path to the included paths in `tsconfig.json` so it can be compiled

    ```
    "include": [
      "node_modules/video-content/src/*"
    ]
    ```

4. Import the package inside an `TypeScript` file and compile that file

    ```js
    import {videoContent} from "video-content/src/index";

    videoContent.init();
    ```

5. Import `video-content.scss` inside the application's sass file or compile and link it inside the document

    ```
    @import "node_modules/video-content/src/video-content.scss";
    ```

### TODO
##### High
-[ ] Find a valid project/folder structure for the `TypeScript/JavaScript` application
-[ ] Define application version in `package.json`

#### Medium
-[ ] Add `TypeScript` compile instructions to README
    - [How to set up a TypeScript project](https://medium.freecodecamp.org/how-to-set-up-a-typescript-project-67b427114884)
-[ ] Move the `alert` related functions to an external module, for example create a project called `bootstrap-component`
    - `createAlertElement`
    - `displayAlert`
-[ ] Import the `bootstrap-component` module in `src/index.js`

#### Low
-[ ] Join the input and the output structure, optimizing the rendering time
-[ ] Add loading icon while rendering the video-content in the view

### Input structure example
```html
<section class="video-container">
    <video-content authors="Author 1, Author 2"
                   layout="vertical"
                   references="Event, City, State, Country"
                   video-url="https://player.vimeo.com/video/323491537">
        <div class="video-content-placeholder">
            <div class="video-icon fa fa-vimeo fa-3x">&nbsp;</div>
        </div>

        <h6 class="video-title">Title of the video</h6>

        <div class="row row-no-gutters">
            <div class="col-sm-12">
                <div class="video-preview-placeholder col-sm-12">&nbsp;</div>
            </div>

            <div class="video-chapters col-sm-12">
                <div class="video-chapter">
                    <div class="video-chapter-seconds">10</div>

                    <div class="video-chapter-title">Title of the first chapter
                    </div>
                </div>

                <div class="video-chapter">
                    <div class="video-chapter-seconds">50</div>

                    <div class="video-chapter-title">Title of the second chapter
                    </div>
                </div>

                <div class="video-chapter">
                    <div class="video-chapter-seconds">100</div>

                    <div class="video-chapter-title">Title of the third chapter
                    </div>
                </div>
            </div>
        </div>
    </video-content>
</section>
```

### Output structure example
```html
<section class="video-container">
    <video-content authors="Author 1, Author 2"
                   layout="vertical"
                   references="Event, City, State, Country"
                   video-url="https://player.vimeo.com/video/323491537">
        <div class="row">
            <div class="col-sm-12">
                <div class="video-responsive-container">
                    <iframe src="https://player.vimeo.com/video/323491537"
                            frameborder="0"
                            webkitallowfullscreen="true"
                            mozallowfullscreen="true"
                            allowfullscreen="true"
                            id="video-video-0"
                            class="video-embedded"
                            data-ready="true"></iframe>
                </div>

                <p class="video-authors">Author 1, Author 2</p>

                <small class="video-references text-muted">Event, City, State, Country</small>

                <h2 class="video-title">Title of the video</h2>
            </div>

            <div class="col-sm-12">
                <ul class="list-group">
                    <li class="list-group-item video-set-time" data-time="10">
                        <h3 class="video-chapter-title">Title of the first chapter</h3>
                        <small class="video-chapter-seconds">10s</small>
                    </li>
                    <li class="list-group-item video-set-time" data-time="50">
                        <h3 class="video-chapter-title">Title of the second chapter</h3>
                        <small class="video-chapter-seconds">50s</small>
                    </li>
                    <li class="list-group-item video-set-time" data-time="100">
                        <h3 class="video-chapter-title">Title of the third chapter</h3>
                        <small class="video-chapter-seconds">100s</small>
                    </li>
                </ul>
            </div>
        </div>
    </video-content>
</section>
```
