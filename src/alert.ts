export function createAlertElement(message: string) {
    let alertElement = document.createElement('div');
    alertElement.classList.add('alert');
    alertElement.classList.add('alert-danger');
    alertElement.classList.add('alert-dismissible');
    alertElement.setAttribute('role', 'alert');

    const buttonElement = createDismissButton();

    let messageElement = document.createElement('p');
    messageElement.innerHTML = message;

    alertElement.append(buttonElement);
    alertElement.append(messageElement);

    return alertElement;
}

export function displayAlert(message: string, element?: HTMLElement, elementSelector: string = '.container') {
    let alertElement = createAlertElement(message);

    let appendTo = document.querySelector(elementSelector);
    if (element instanceof HTMLElement) {
        appendTo = element;
    }

    if (appendTo === null) {
        return;
    }

    appendTo.append(alertElement);
}

function createDismissButton() {
    const buttonElement = document.createElement('button');
    buttonElement.type = 'button';
    buttonElement.classList.add('close');
    buttonElement.dataset.dismiss = 'alert';
    buttonElement.setAttribute('aria-label', 'Close');

    const closeElement = document.createElement('span');
    closeElement.setAttribute('aria-hidden', 'true');
    closeElement.innerHTML = '&times;';

    buttonElement.prepend(closeElement);

    addDismissListener(buttonElement);

    return buttonElement;
}

function addDismissListener(element: HTMLElement) {
    const hasJquery = function (windowObject: any) {
        return typeof windowObject.$ !== 'undefined'
            || typeof windowObject.jQuery !== 'undefined';
    };

    if (hasJquery(window)) {
        return;
    }

    element.addEventListener('click', () => {
        const parentElement = element.parentElement;
        if (!parentElement) {
            return;
        }

        parentElement.remove();
    });
}
