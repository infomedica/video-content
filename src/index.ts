import {displayAlert} from "./alert";
import VideoContentElement from './video-content.element';
import VideoContentComponent from './video-content.component';
import VideoContent from './video-content';
import Chapter from "./chapter";

/**
 * App element selector
 */
const elementSelector = 'video-content';

let appErrors: Array<Error> = [];

/**
 * Display all errors at once
 */
const displayErrors = () => {
    if (appErrors.length === 0) {
        return false;
    }

    appErrors.forEach((error) => {
        displayAlert(error.message);
    });
};

const init = () => {
    // Define a new custom element
    window.customElements.define(elementSelector, VideoContentElement);

    // customElements
    //     .whenDefined('temp-content')
    //     .then(() => {});

    const components = document.querySelectorAll(elementSelector);
    if (components.length === 0) {
        return false;
    }

    // Count the components
    console.log(`'${elementSelector}' components found: ${components.length}`);

    const getTitle = (videoContentElement: VideoContentElement) => {
        const videoTitleElement: HTMLDivElement | null = videoContentElement.querySelector('.video-title');
        let videoTitle = '';

        if (videoTitleElement !== null) {
            videoTitle = videoTitleElement.innerText;
        }

        return videoTitle;
    };

    const getChapters = (videoContentElement: VideoContentElement) => {
        let chapterElements = videoContentElement.querySelectorAll('.video-chapter');

        let chapters: Chapter[] = [];

        const pushChapter = (chapterElement: HTMLElement) => {
            const secondsElement: HTMLElement | null = chapterElement.querySelector('.video-chapter-seconds');
            let seconds = 0;

            if (secondsElement !== null) {
                seconds = parseInt(secondsElement.innerText);
            }

            const titleElement: HTMLDivElement | null = chapterElement.querySelector('.video-chapter-title');
            let title = '';

            if (titleElement !== null) {
                title = titleElement.innerText;
            }

            chapters.push(new Chapter(title, seconds));
        };

        chapterElements.forEach(pushChapter);

        return chapters;
    };

    const loadVideoContent = (videoContentElement: VideoContentElement, index: number) => {
        const videoTitle = getTitle(videoContentElement);
        const chapters = getChapters(videoContentElement);

        let videoUrl = videoContentElement.videoUrl;
        if (typeof videoUrl === 'undefined') {
            const newVideoUrl = videoContentElement.getAttribute('video-url');
            videoUrl = newVideoUrl !== null ? newVideoUrl : '';
        }

        let layout = videoContentElement.layout;
        if (typeof layout === 'undefined') {
            const newLayout = videoContentElement.getAttribute('layout');
            layout = newLayout !== null ? newLayout : '';
        }

        let authors = videoContentElement.authors;
        if (typeof authors === 'undefined') {
            const newAuthors = videoContentElement.getAttribute('authors');
            authors = newAuthors !== null ? newAuthors : '';
        }

        let references = videoContentElement.references;
        if (typeof references === 'undefined') {
            const newReferences = videoContentElement.getAttribute('references');
            references = newReferences !== null ? newReferences : '';
        }

        const videoContent = new VideoContent(
            index,
            videoUrl,
            layout,
            videoTitle,
            authors,
            references,
            chapters
        );

        // TODO: Refactor so that the rendered content can be used instead or re-rendering it
        videoContentElement.innerHTML = '';

        const videoComponent = new VideoContentComponent(videoContent, videoContentElement);
        videoComponent.load();
    };

    const tryCatchOnLoad = (videoContentElement, index) => {
        try {
            loadVideoContent(videoContentElement, index);
        } catch (error) {
            let heading = `Error in ${elementSelector} number ${(index + 1)}:`;
            let message = `<strong>${heading}</strong> <br> ${error.message}`;

            displayAlert(message, videoContentElement);

            return false;
        }
        return true;
    };

    components.forEach(tryCatchOnLoad);
};

// Document Ready
// window.addEventListener('DOMContentLoaded', runApp);
export const videoContent = {
    init,
};
