/**
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements}
 *
 * Note:
 * Custom attributes do not work if the custom element class extends the HTMLDivElement
 * e.g.
 *      class ExampleElement extends HTMLDivElement {...}
 *      customElements.define('example-element', ExampleElement, {extends: 'div'});
 */

// import Chapter from './chapter';

export default class VideoContentElement extends HTMLElement {
    // Attribute names
    static videoUrl = 'video-url';
    // static videoTitle = 'video-title';
    static layout = 'layout';
    static authors = 'authors';
    static references = 'references';
    // static chapters = 'chapters';

    static requiredAttributeNames = [
        VideoContentElement.videoUrl,
        // VideoContentElement.videoTitle,
        // VideoContentElement.chapters
    ];

    constructor() {
        // Call super first in constructor
        super();

        // this.attachShadow({ mode: "open" });
    }

    // static get observedAttributes() {
    //     return ['layout'];
    // }

    // connectedCallback() {
    // }

    getValue(attributeName: string): string {
        let value = this.getAttribute(attributeName);

        if (
            (value === null || value.length < 1)
            && VideoContentElement.requiredAttributeNames.includes(attributeName)
        ) {
            throw new Error(`Attribute '${attributeName}' cannot be empty.`);
        }

        return typeof value === 'string' ? value : '';
    }

    get videoUrl(): string {
        let videoUrl = this.getValue(VideoContentElement.videoUrl);

        return VideoContentElement.validateVideoUrl(videoUrl);
    }

    get layout(): string {
        return this.getValue(VideoContentElement.layout);
    }

    // get videoTitle(): string {
    //     return this.getValue(VideoContentElement.videoTitle);
    // }

    get authors(): string {
        return this.getValue(VideoContentElement.authors);
    }

    get references(): string {
        return this.getValue(VideoContentElement.references);
    }

    // get chapters(): Chapter | Array<Chapter> {
    //     // TODO: Remove one of the type check here or in video-content.ts
    //     let rawChapters = this.getValue(VideoContentElement.chapters);
    //     let objectChapters: Chapter | Array<Chapter> = JSON.parse(rawChapters);
    //
    //     const pushChapter = (chapter: Chapter) => {
    //         chapters.push(new Chapter(chapter.title, chapter.seconds));
    //     };
    //
    //     let chapters: Array<Chapter> = [];
    //     if(Array.isArray(objectChapters)) {
    //         objectChapters.forEach(pushChapter);
    //     } else {
    //         pushChapter(objectChapters);
    //     }
    //
    //     return chapters;
    // }

    // static validateChapters(chapters: string) {
    //     try {
    //         return JSON.parse(chapters);
    //     } catch (error) {
    //         let message = `Expected JSON, got '${chapters}'.`;
    //         message += '\n';
    //
    //         throw new TypeError(message + error);
    //     }
    // }

    static validateVideoUrl(videoUrl: string): string {
        try {
            let url = new URL(videoUrl);
            return url.href;
        } catch (error) {
            let message = `Attribute '${VideoContentElement.videoUrl}' must be a valid url, got '${videoUrl}'.`;
            message += '\n';

            throw new Error(message + error);
        }
    }

    set videoUrl(videoUrl: string) {
        let validVideoUrl = VideoContentElement.validateVideoUrl(videoUrl);

        this.setAttribute(VideoContentElement.videoUrl, validVideoUrl);
    }

    // set chapters(chapters: Chapter | Array<Chapter>) {
    //     let jsonChapters = JSON.stringify(chapters);
    //
    //     this.setAttribute(VideoContentElement.chapters, jsonChapters);
    // }
}
