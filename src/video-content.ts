import Chapter from './chapter';

const isEmptyString = (value) =>  {
    return value === '';
};

export default class VideoContent {
    static horizontalLayout:string = 'horizontal';
    static verticalLayout:string = 'vertical';
    static layouts: string[] = [VideoContent.horizontalLayout, VideoContent.verticalLayout];
    static defaultLayout: string = VideoContent.horizontalLayout;

    id: number;
    videoUrl: string;
    layout: string;
    title: string;
    authors: string;
    references: string;
    private _chapters: Chapter | Array<Chapter>;

    // TypeScript prop
    constructor(
        id: number,
        videoUrl: string,
        layout = VideoContent.defaultLayout,
        title: string = '',
        authors: string = '',
        references: string = '',
        chapters: Chapter | Array<Chapter>
    ) {
        this.id = id;
        this.videoUrl = videoUrl;
        this.layout = layout;
        this.title = title;
        this.authors = authors;
        this.references = references;
        this.chapters = chapters;
    }

    /**
     * @see {@link https://javascript.info/property-accessors#smarter-getters-setters}
     * @see {@link https://www.typescriptlang.org/docs/handbook/classes.html#accessors}
     */
    get chapters() {
        return this._chapters;
    }

    set chapters(value: Chapter|Array<Chapter>|string) {
        // Check if is Chapter type, if true assign it to _chapters and return
        if (value instanceof Chapter) {
            this._chapters = value;
            // TODO - TypeScript: What should be returned?
            return;
        }

        // No need to perform
        // if (!Array.isArray(value) && typeof value !== 'object' && typeof value !== 'string') {
        //     throw new TypeError("chapters must be an 'array', an object or a 'string'");
        // }

        if (typeof value === 'string') {
            value = JSON.parse(value);
        }

        let validChapters:Array<Chapter> = [];

        const pushChapter = (oneValue) => {
            const title: string = oneValue.title;
            const seconds: number = oneValue.seconds;
            let chapter = new Chapter(title, seconds);
            validChapters.push(chapter);
        };

        // if(!Array.isArray(value) && typeof value === 'object') {
        //     let chapter = new Chapter(value.title, value.seconds);
        //     validChapters.push(chapter);
        // } else if (Array.isArray(value)) {
        if (value instanceof Array) {
            value.forEach(pushChapter);
        } else {
            pushChapter(value);
        }

        this._chapters = validChapters;
    }

    static isLayout(layout: string) {
        return VideoContent.layouts.includes(layout);
    }

    static isHorizontalLayout(layout: string) {
        return layout === VideoContent.horizontalLayout;
    }

    static isVerticalLayout(layout: string) {
        return layout === VideoContent.verticalLayout;
    }

    hasHorizontalLayout() {
        return VideoContent.isHorizontalLayout(this.layout);
    }

    hasVerticalLayout() {
        return VideoContent.isVerticalLayout(this.layout);
    }

    isTitleEmpty() {
        return isEmptyString(this.title);
    }

    isAuthorsEmpty() {
        return isEmptyString(this.authors);
    }

    isReferencesEmpty() {
        return isEmptyString(this.references);
    }

    isAdditionalContentsEmpty() {
        return this.isTitleEmpty()
            && this.isAuthorsEmpty()
            && this.isReferencesEmpty();
    }
}
