import VideoContent from "./video-content";
import Chapter from './chapter';
import VideoContentElement from "./video-content.element";
import Player from '@vimeo/player';

export default class VideoContentComponent {
    static prefix = 'video';

    content: VideoContent;
    element: VideoContentElement;
    vimeoPlayer: Player;
    loadingIconElement: HTMLElement;

    constructor(content: VideoContent, element: VideoContentElement) {
        this.content = content;
        this.element = element;
    }

    load() {
        this.appendLoadingIcon()
            .removePlaceHolder();

        let layoutElement = this.createLayout();
        this.element.append(layoutElement);

        // const shadowRoot = this.element.attachShadow({mode: 'open'});
        // shadowRoot.append(this.getCss());
        // shadowRoot.append(layoutElement);

        // Load the video
        this.vimeoPlayer = new Player(this.getVimeoElementId());

        // TODO: Research this ambiguity inside a function
        let thisObject = this;
        this.vimeoPlayer
            .ready()
            .then(() => {
                thisObject.listenSetTimeEvent();

                thisObject.removeLoadingIcon();
            });
    }

    static applyPrefix(...args: Array<string>) {
        const separator = '-';

        return VideoContentComponent.prefix + separator + args.join(separator);
    }

    // TODO: TypeScript chose to keep the 'get' in 'getFunctionName' or replace it with 'get functionName()'
    static getLoadingIconClass() {
        return VideoContentComponent.applyPrefix('loading-icon');
    }

    appendLoadingIcon() {
        let loadingIconElement = document.createElement('div');
        loadingIconElement.className = VideoContentComponent.getLoadingIconClass();
        loadingIconElement.innerHTML = 'Loading...';

        this.loadingIconElement = loadingIconElement;

        this.element.append(this.loadingIconElement);

        return this;
    }

    removeLoadingIcon() {
        this.loadingIconElement.remove();

        return this;
    }

    removePlaceHolder() {
        let placeHolderSelector = '.' + VideoContentComponent.applyPrefix('content-placeholder');
        let pleaceHolderElement = this.element.querySelector(placeHolderSelector);

        if (pleaceHolderElement instanceof HTMLElement) {
            pleaceHolderElement.remove();
        }

        return this;
    }

    createLayout() {
        let row = document.createElement('div');
        row.classList.add('row');
        // row.classList.add('row-no-gutters');

        let videoContainer = document.createElement('div');
        let chaptersContainer = document.createElement('div');

        let videoContainerClass = 'col-sm-7';
        let chaptersContainerClass = 'col-sm-5';

        let authorsContainer = chaptersContainer;
        let titleContainer = chaptersContainer;

        // TODO: Add titleContainer and append references
        if (this.content.hasVerticalLayout()) {
            const fullWidthColumnClass = 'col-sm-12';

            videoContainerClass = fullWidthColumnClass;
            chaptersContainerClass = fullWidthColumnClass;

            titleContainer = authorsContainer;
        }

        videoContainer.className = videoContainerClass;
        this.appendVimeo(videoContainer);

        this.appendAdditionalContents(authorsContainer, titleContainer);

        chaptersContainer.className = chaptersContainerClass;
        // TODO: change method name to appendChaptersTo()
        this.appendChapters(chaptersContainer);

        row.append(videoContainer, chaptersContainer);

        return row;
    }

    getVimeoElementId() {
        return VideoContentComponent.applyPrefix('video-' + this.content.id);
    }

    appendVimeo(element) {
        let vimeoContainer = document.createElement('div');
        vimeoContainer.className = VideoContentComponent.applyPrefix('responsive-container');

        let vimeoElement = VideoContentComponent.createVimeoIframe(this.content.videoUrl);
        vimeoElement.id = this.getVimeoElementId();
        vimeoElement.className = VideoContentComponent.applyPrefix('embedded');

        vimeoContainer.append(vimeoElement);

        element.append(vimeoContainer);

        return this;
    }

    static createVimeoEmbedded(videoUrl: string): HTMLDivElement {
        let vimeoEmbedded = document.createElement('div');
        vimeoEmbedded.dataset.vimeoUrl = videoUrl;

        return vimeoEmbedded;
    }

    static createVimeoIframe(videoUrl: string): HTMLIFrameElement {
        let vimeoIframe = document.createElement('iframe');
        vimeoIframe.src = videoUrl;
        vimeoIframe.frameBorder = '0';
        vimeoIframe.setAttribute('webkitallowfullscreen', 'true');
        vimeoIframe.setAttribute('mozallowfullscreen', 'true');
        vimeoIframe.setAttribute('allowfullscreen', 'true');
        vimeoIframe.setAttribute('allow', 'autoplay');

        return vimeoIframe;
    }

    createChapterItem(chapter: Chapter): HTMLElement {
        let listItemElement = document.createElement('li');

        listItemElement.className = 'list-group-item';
        const setTimeClass = VideoContentComponent.applyPrefix('set-time');
        listItemElement.classList.add(setTimeClass);

        listItemElement.dataset.time = chapter.seconds.toString();

        /**
         * Create an element by passing a chapter property name
         */
        const createPropertyElement = (tagName:string, propertyName: string): HTMLElement => {
            let propertyElement = document.createElement(tagName);
            propertyElement.className = VideoContentComponent.applyPrefix('chapter-' + propertyName);

            return propertyElement;
        };

        let titleElement = createPropertyElement('h3', 'title');
        titleElement.innerHTML = chapter.title;

        let secondsElement = createPropertyElement('small', 'seconds');
        secondsElement.innerHTML = `${chapter.seconds}s`;

        listItemElement.append(
            titleElement,
            secondsElement
        );

        return listItemElement;
    }

    createChaptersList(chapters: Chapter[]): HTMLUListElement {
        let chaptersListElement = document.createElement('ul');
        chaptersListElement.className = 'list-group';

        // Append item to the list
        chapters.forEach((chapter: Chapter) => {
            let listItemElement = this.createChapterItem(chapter);

            chaptersListElement.append(listItemElement);
        });

        return chaptersListElement;
    }

    appendChapters(parentNode: ParentNode) {
        // TODO: Set type of chapters to 'Chapter[]'
        let chapters = this.content.chapters;
        if (Array.isArray(chapters) && chapters.length > 0) {
            let chaptersListElement = this.createChaptersList(chapters);

            // this.element.append(chaptersListElement);
            parentNode.append(chaptersListElement);
        }

        return this;
    }

    // static createElementWithInnerHTML(tagName: string, innerHTML: string) {
    //     let element = document.createElement(tagName);
    //     element.innerHTML = innerHTML;
    //
    //     return element;
    // }

    appendAdditionalContents(authorsContainer: HTMLElement, titleContainer: HTMLElement) {
        if (!this.content.isAdditionalContentsEmpty()) {
            this.appendAuthors(authorsContainer)
                .appendReferences(authorsContainer)
                .appendTitle(titleContainer);
        }

        return this;
    }

    appendTitle(parentNode: ParentNode) {
        if (!this.content.isTitleEmpty()) {
            let titleElement = document.createElement('h2');
            titleElement.classList.add(VideoContentComponent.applyPrefix('title'));
            titleElement.innerHTML = this.content.title;
            parentNode.append(titleElement);
        }

        return this;
    }

    appendAuthors(parentNode: ParentNode) {
        if (!this.content.isAuthorsEmpty()) {
            let authorsElement = document.createElement('p');
            authorsElement.classList.add(VideoContentComponent.applyPrefix('authors'));
            authorsElement.innerHTML = this.content.authors;

            parentNode.append(authorsElement);
        }

        return this;
    }

    appendReferences(parentNode: ParentNode) {
        if (!this.content.isReferencesEmpty()) {
            let referencesElement = document.createElement('small');
            referencesElement.classList.add(VideoContentComponent.applyPrefix('references'));
            referencesElement.classList.add('text-muted');
            referencesElement.innerHTML = this.content.references;

            parentNode.append(referencesElement);
        }

        return this;
    }

    listenSetTimeEvent() {
        let thisObject = this;

        const setTimeSelector = '.' + VideoContentComponent.applyPrefix('set-time');

        const addClickEvent = (liElement: HTMLLIElement) => {
            liElement.addEventListener('click', () => {
                const seconds = Number.parseInt(<string>liElement.dataset.time);
                thisObject.setTime(seconds);
            });
        };

        this.element
            .querySelectorAll(setTimeSelector)
            .forEach(addClickEvent);
    }

    private setTime(seconds: number) {
        let thisObject = this;

        const playVideo = (seconds: number) => {
            thisObject.vimeoPlayer.play();
        };

        this.vimeoPlayer.setCurrentTime(seconds)
            .then(playVideo)
            .catch((error) => {
                if (error.name === 'RangeError') {
                    // The time was less than 0 or greater than the video’s duration
                    console.log(error.name);
                }
            });
    }
}
