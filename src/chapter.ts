const className = 'Chapter';

export default class Chapter {
    private _title: string;
    // TODO: Search for only positive integer type (seconds > 0)
    private _seconds: NonNullable<number>;

    constructor(title: string, seconds: NonNullable<number>) {
        this.title = title;
        this.seconds = seconds;
    }

    get title(): string {
        return this._title;
    }

    set title(newTitle: string) {
        // if (typeof newTitle !== 'string') {
        //     throw new Error(`${className}.title must be a 'string'.`);
        // }
        this._title = newTitle;
    }

    get seconds(): number {
        return this._seconds;
    }

    set seconds(newSeconds: NonNullable<number>) {
        if (!Number.isInteger(newSeconds)) {
            throw new Error(`${className}.seconds must be an 'integer'.`);
        }

        this._seconds = newSeconds;
    }

    /**
     * Call the getter of a given property.
     *
     * @see {@link https://ctrlq.org/code/20181-call-javascript-function-by-name}
     */
    // getValue(propertyName: string): string | number {
    //     const value = this[propertyName];
    //
    //     if (typeof value === 'undefined') {
    //         throw new Error('Property \'' + propertyName + '\' not found');
    //     }
    //
    //     return value;
    // }
}
